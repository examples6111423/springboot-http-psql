## Springboot Backend Example 

This is part of a fullstack Webapplication build with java and springboot with a postgresql DB in the Backend.

### SetUp (Prerequisites)

* JDK 17 or higher
* an IDE (inteliJ, Eclipse, VS Code ...)
* Docker installation to run the postgres DB in Docker

### Init the springboot application

A very useful tool is the the spring intializer https://start.spring.io which is also included in inteliJ.

For this very basic setup use the default settings (gradle, bootversion, java) as recommended by the initializer.

__Required Dependencies:__
* org.springframework.boot:spring-starter-web
* org.springframework.boot:spring-starter-data-jpa
* org.postgresql:postgresql (runtimeOnly)

_Optional:_
* org.springdoc:springdoc-openapi-starter-webmvc-ui:2.0.4
_(this adds an open api spec and the swagger ui to the project)_

This is how the build.gradle dependencies section looks like:

```
dependencies {

    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-web'

    runtimeOnly 'org.postgresql:postgresql'

    // api doc
    implementation 'org.springdoc:springdoc-openapi-starter-webmvc-ui:2.0.4'

    testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

### Useful stuff

* https://spring.io/guides/gs/spring-boot/ _(getting started with springboot)_
* https://www.spring-boot.io/blog/spring-boot-und-postgresql _(Springboot using postgresql)_