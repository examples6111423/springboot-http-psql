package com.example.demo.feedback;

import java.time.LocalDateTime;

public class FeedbackDto {

    private String id;

    private String name;

    private Double stars;

    private String comment;

    private LocalDateTime date;


    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Double getStars() {
        return stars;
    }

    public void setStars(final Double stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(final LocalDateTime date) {
        this.date = date;
    }
}
