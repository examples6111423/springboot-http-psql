package com.example.demo.feedback;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    private final FeedbackService feedbackService;

    public FeedbackController(final FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @PostMapping
    public ResponseEntity<FeedbackDto> saveFeedback(@RequestBody final FeedbackDto feedbackDto) {
        FeedbackDto createdFeedback = this.feedbackService.saveFeedback(feedbackDto);
        return ResponseEntity.ok(createdFeedback);
    }

    @GetMapping
    public ResponseEntity<List<FeedbackDto>> list() {
        return ResponseEntity.ok(this.feedbackService.getFeedbacks());
    }

    @GetMapping("/{id}")
    public ResponseEntity<FeedbackDto> getById(@PathVariable final String id) {
        Optional<FeedbackDto> feedback = this.feedbackService.getFeedback(id);
        return feedback
                .map(it -> ResponseEntity.ok(it))
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Boolean> delete(@PathVariable final String id) {
        boolean deleted = this.feedbackService.delete(id);
        return ResponseEntity.ok(deleted);
    }

}
