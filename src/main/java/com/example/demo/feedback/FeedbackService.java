package com.example.demo.feedback;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class FeedbackService {

    private final FeedbackRepository feedbackRepository;

    public FeedbackService(final FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    public FeedbackDto saveFeedback(final FeedbackDto feedbackDto) {
        FeedbackEntity feedback = new FeedbackEntity(feedbackDto);
        feedback = this.feedbackRepository.save(feedback);
        return feedback.toDto();
    }

    public List<FeedbackDto> getFeedbacks() {
        List<FeedbackEntity> feedbackEntities = (List<FeedbackEntity>) this.feedbackRepository.findAll();
        return feedbackEntities.stream().map(it -> it.toDto()).collect(Collectors.toList());
    }

    public Optional<FeedbackDto> getFeedback(String id) {
        Optional<FeedbackEntity> feedback = this.feedbackRepository.findById(UUID.fromString(id));
        return Optional.ofNullable(feedback.map(it -> it.toDto()).orElse(null));
    }

    public boolean delete(String id) {
        try {
            this.feedbackRepository.deleteById(UUID.fromString(id));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
