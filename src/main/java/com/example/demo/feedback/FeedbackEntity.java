package com.example.demo.feedback;

import java.util.Optional;
import java.util.UUID;

import com.example.demo.infrastructure.BaseEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "feedback")
public class FeedbackEntity extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Double stars;

    @Column
    private String comment;

    public FeedbackEntity() {
    }

    public FeedbackEntity(FeedbackDto feedbackDto) {
        Optional.ofNullable(feedbackDto.getId())
            .ifPresent(id -> this.setId(UUID.fromString(id)));
        this.name = feedbackDto.getName();
        this.stars = feedbackDto.getStars();
        this.comment = feedbackDto.getComment();
    }

    public FeedbackDto toDto() {
        final FeedbackDto feedbackDto = new FeedbackDto();
        Optional.ofNullable(this.getId())
            .ifPresent(id -> feedbackDto.setId(id.toString()));
        feedbackDto.setName(this.getName());
        feedbackDto.setStars(this.getStars());
        feedbackDto.setComment(this.getComment());
        feedbackDto.setDate(this.getModifyDate());
        return feedbackDto;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Double getStars() {
        return stars;
    }

    public void setStars(final Double stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }
}
