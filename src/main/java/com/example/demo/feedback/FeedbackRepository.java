package com.example.demo.feedback;

import java.util.UUID;

import com.example.demo.infrastructure.CrudPagingRepository;

public interface FeedbackRepository extends CrudPagingRepository<FeedbackEntity, UUID> {
}
