## Run a postgres Server with docker

### Without Docker installed

you will need a running docker installation on your system.
see https://docs.docker.com/engine/install/

### With Docker installed

If you have not used a postgres docker image before, 
please run

```docker pull postgres```

For this demo application you can easily run the postgres
server using the runpsql.sh script. it runs a postgres server
in docker.

The Script starts the official postgres image. 
The script expects a path where to persist the db-data on
the docker host system. 

Example Usage and the postgres environment vars, which are set.
```
./runpsql.sh ~/development/workshops/fullstack/dbdata
- port : 5432
- initial db : demo
- user : dbuser
- password : geheim 
```

__!!!NEVER USE THIS FOR PORDUCTION!!!__

See further Information about using docker to run a postgres db server
https://www.docker.com/blog/how-to-use-the-postgres-docker-official-image/
